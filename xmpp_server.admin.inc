<?php

/**
 * Form builder. Configure XMPP server settings.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function xmpp_server_admin_settings() {
  $form = array();

  $form['xmpp_server_cache_lists'] = array(
    '#title' => t('List caching'),
    '#type' => 'checkbox',
    '#title' => t('List caching'),
    '#description' => t('Set whether or not the XMPP server should cache it\'s roster or presence lists. (May hinder development.)'),
    '#default_value' => variable_get('xmpp_server_cache_lists', false),
  );

  $frequency = variable_get('xmpp_server_ping_frequency', 15);
  $running = (variable_get('xmpp_server_ping', 0) > time() - $frequency) && variable_get('xmpp_server_running', false);

  $form['xmpp_server_running'] = array(
    '#title' => t('Running'),
    '#type' => 'checkbox',
    '#description' => t('Set whether or not the XMPP Server should continue running. (Only for stopping the server.)'),
    '#default_value' => $running,
  );

  /* Runtime Settings */
  $form['xmpp_server_host_settings'] = array(
    '#title' => t('XMPP Server runtime settings'),
    '#type' => 'fieldset',
    '#description' => t('Settings for the XMPP server\'s runtime behavior.'),
  );

  $form['xmpp_server_host_settings']['xmpp_server_ping_frequency'] = array(
    '#title' => t('Ping frequency'),
    '#type' => 'textfield',
    '#description' => t('This value controls the number of seconds that the XMPP server will wait before synchronizing it\'s data with the website. Higher values will increase performance at the cost of accuracy.'),
    '#default_value' => variable_get('xmpp_server_ping_frequency', 15),
    '#size' => 3,
  );

  $form['xmpp_server_host_settings']['xmpp_server_host'] = array(
    '#title' => t('XMPP Server Hostname'),
    '#type' => 'textfield',
    '#description' => t('The hostname that the XMPP Server will identify itself as. WARNING: Changing this could cause XMPP clients to behave poorly.'),
    '#default_value' => variable_get('xmpp_server_host', $_SERVER['HTTP_HOST']),
  );

  /* Connection Settings */

  //Collapse the form if it's elements have been set already.
  $conset = (variable_get('xmpp_server_port', false) && variable_get('xmpp_server_address', false)) ? TRUE : FALSE;
  $form['xmpp_server_connection'] = array(
    '#title' => t('XMPP Server connection settings'),
    '#type' => 'fieldset',
    '#description' => t('Client connection settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => $conset,
  );

  $form['xmpp_server_connection']['xmpp_server_address'] = array(
    '#title' => t('XMPP Server address'),
    '#type' => 'textfield',
    '#title' => t('XMPP Server address'),
    '#description' => t('The address that your XMPP server should bind to.'),
    '#default_value' => variable_get('xmpp_server_address', '0.0.0.0'),
  );

  $form['xmpp_server_connection']['xmpp_server_port'] = array(
    '#title' => t('XMPP Server port'),
    '#type' => 'textfield',
    '#title' => t('XMPP Server port'),
    '#description' => t('The port that your XMPP server will run on. (Default is 5222.)'),
    '#default_value' => variable_get('xmpp_server_port', 5222),
    '#size' => 5,
  );

  /* SSL settings */

  $form['xmpp_server_ssl'] = array(
    '#title' => t('SSL Settings'),
    '#type' => 'fieldset',
    '#description' => t('Settings that affect the behavior of XMPP encryption.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['xmpp_server_ssl']['xmpp_server_ssl_enabled'] = array(
    '#title' => t('Enabled'),
    '#type' => 'checkbox',
    '#description' => t('Check to enable SSL support, or uncheck to disable.'),
    '#default_value' => variable_get('xmpp_server_ssl_enabled', false),
  );

  $form['xmpp_server_ssl']['xmpp_server_ssl_local_cert'] = array(
    '#title' => t('Local Certificate'),
    '#type' => 'textfield',
    '#description' => t('The path to your SSL certificate. This must be in PEM format.'),
    '#default_value' => variable_get('xmpp_server_ssl_local_cert', ''),
  );

  $form['xmpp_server_ssl']['xmpp_server_ssl_cafile'] = array(
    '#title' => t('Certificate Authority File'),
    '#type' => 'textfield',
    '#description' => t('The path to your Certificate authority file. This will probably be the same file as your local cert.'),
    '#default_value' => variable_get('xmpp_server_ssl_cafile', ''),
  );

  $form['xmpp_server_ssl']['xmpp_server_ssl_capath'] = array(
    '#title' => t('CA Path'),
    '#type' => 'textfield',
    '#description' => t('The path to your CA files. This will probably be the directory that contains your local cert.'),
    '#default_value' => variable_get('xmpp_server_ssl_capath', ''),
  );

  return system_settings_form($form);
}

