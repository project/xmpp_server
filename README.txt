 README.txt
============

The XMPP Server module for Drupal provides a realtime server, written in
PHP, which communicates to clients over the XMPP protocol, using Drupal as a
source for authentication information.


 INSTALLATION
==============

1) Perform standard installation of 'XMPP Server' module, the 'XMPP Handler'
module, and the 'XMPP Roster' module. This will provide the core
functionality of the XMPP server.

2) Copy xmppd.ini.example to xmppd.ini.

3) Read xmppd.ini and change settings to match your Drupal installation.

4) Run the file 'run.sh' to start the server.


 CONTRIBUTION NOTES
====================

The XMPP Server module was built using Drupal's hooks/callbacks system, which
makes it easy to extend the protocol that it supports with contrib modules.
However, there are certain things that developers must be aware of when
developing modules to add functionality to XMPP. Reading this section can save
lots of headaches.

First of all, the XMPP Server is a realtime service. Optimally it will never
be restarted. Though it is possible, Drupal was not designed to operate in
this manner. Take a look at the function user_access(). A feature of this
function is to cache permissions that have already been retrieved in a static
variable. This is a performance benefit in a page load on a website, and a bug
in a realtime service. When developing contrib modules, know what code is
being executed in the realtime service, and make sure that any data that is
cached in this manner is reset every time you make a call.

Secondly, avoid reusing code between the website and the XMPP server. This is
not a hard-and-fast rule, but it will help save confusion. Some things simply
aren't functional in the context of the server vs. the website.
drupal_set_message(), for example, will do nothing in the server context.
Likewise, attempting to retrieve a client from the website will fail because
the clients do not exist in the context of a page load. Know the API, and know
the proper way of sending data back and forth between the website and the
server.


 AUTHOR/MAINTAINER
===================
~Sean Edwards <edwards AT notsorandom DOT com>
Not So Random
http://www.notsorandom.com/site/xmpp-server

