<?php

$_xmpp_server_timers = array();

function _xmpp_server_run_timers() {
  global $_xmpp_server_timers;
  foreach ($_xmpp_server_timers as $id => $timer) {
    if (time() > $timer['next']) {
      if (function_exists($timer['func'])) {
        $ret = call_user_func_array($timer['func'], $timer['args']);
        if (is_numeric($ret)) {
          $timer['delay'] = $ret;
        }
        $timer['next'] += $timer['delay'];
      }
      else {
        xmpp_server_cancel_timer($id);
      }
    }
  }
}

function xmpp_server_new_timer($delay, $func, $args = array()) {
  static $id;
  global $_xmpp_server_timers;
  $id++;
  $_xmpp_server_timers[$id] = array(
    'delay' => $delay,
    'next' => time() + $delay,
    'func' => $func,
    'args' => $args,
  );
  return $id;
}

function xmpp_server_cancel_timer($id) {
  global $_xmpp_server_timers;
  unset($_xmpp_server_timers[$id]);
}
