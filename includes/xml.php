<?php

class XmlNode {
  public $tag = "";
  public $data = "";
  public $children = array();
  public $options = array();
  public $depth = 0;
  public $op = NULL;
}

$xmpp_server_xml_filters = array();

class XmlStreamParser {
  private $depth = 0;
  private $_proto = '';
  private $cache = array();

  function tag_open($parser, $tag, $attrs) {
    $tag = strtolower($tag);
    $this->depth++;
    if($tag == $this->_proto) {
      $this->cache = array();
      $this->depth = 1;
    }

    $node = new XmlNode();
    $node->tag = $tag;
    $node->data = "";
    $node->children = array();
    $node->options = array();

    foreach ($attrs as $k => $v) {
      $k = strtolower($k);
      $node->options[$k] = $v;
    }

    //If this is the top-level tag...
    if ($this->depth == 1) {
      $this->_proto = $tag;
      $client = xmpp_server_get_client($this, 'xml');
      $ret = module_invoke_all('xmpp_server_protocol_init', $client, $node);
      xmpp_server_send($client, $ret);
    }
    else {
      $this->_set_tag($node);
    }
  }

  function tag_close($parser, $tag) {
    $tag = strtolower($tag);

    $node = $this->_get_tag();

    $node->depth = $this->depth;

    if ($this->depth > 2) {
      $parent = $this->_get_tag(-1);
      $parent->children[] = $node;
    }
    elseif ($this->depth == 2) {
      $client = xmpp_server_get_client($this, 'xml');
      $ret = module_invoke_all('xmpp_server_protocol', $client, $node);
      xmpp_server_send($client, $ret);
    }
    else{
      $client = xmpp_server_get_client($this, 'xml');
      module_invoke_all('xmpp_server_protocol_end', $client);
    }
    unset($this->cache[$this->depth]);

    $this->depth--;
  }

  function cdata($parser, $cdata) {
    if ($this->depth >= 2) {
      $this->_get_tag()->data .= $cdata;
    }
  }

  private function _set_tag($node) {
    $this->cache[$this->depth] = $node;
  }

  private function _get_tag($offset = 0) {
    return $this->cache[$this->depth + $offset];
  }
}

function xmpp_server_start_xml() {
}

function xmpp_server_xml_parser() {
  $parser = new XmlStreamParser;

  //Create a parser
  $p = xml_parser_create();

  xml_set_object($p, $parser);

  //Set element handlers
  xml_set_element_handler($p, array($parser, 'tag_open'), array($parser, 'tag_close'));

  //Set cdata handler
  xml_set_character_data_handler($p, array($parser, 'cdata'));

  $parser->p = $p;

  return $parser;
}

function xmpp_server_xml_stream_parse($client, $data) {
  if (!xml_parse($client->xml->p, $data)) {
    echo "XML error: ". xml_error_string(xml_get_error_code($client->xml)) . "\n";
  }
}

/**
 * Serialize an XML object.
 *
 * @param $node
 *   The node to serialize, as an object or an array.
 * @param $op
 *   The serialization operation to perform. This should be 'open', 'body' or
 *   'close'.
 */
function xmpp_server_xml_serialize($node, $op = NULL) {
  $ret = "";
  
  if (!is_array($node)) {
    $node = (array)$node;
  }

  if ($op == NULL) {
    $op = $node['op'];
  }

  //Serialize the opening tag.
  if ($op == NULL || $op == 'open') {
    $ret .= "<{$node['tag']}";
    foreach ($node['options'] as $k => $v) {
      $ret .= " $k=\"". htmlentities($v) . "\"";
    }

    if ($op == NULL && empty($node['data']) && !count($node['children'])) {
      return $ret . "/>"; //Short tag support.
    }

    $ret .= '>';
  }

  //Serialize our inner content.
  if ($op == NULL || $op == 'body') {
    //Serialize children.
    foreach ($node['children'] as $k => $v) {
      if (is_numeric($k)) {
        //Recursively serialize children.
        $ret .= xmpp_server_xml_serialize($v);
      }
    }

    //Tack on our character data
    $ret .= $node['data'];
  }

  //Serialize the closing tag.
  if ($op == NULL || $op == 'close') {
    $ret .= "</{$node['tag']}>";
  }

  return $ret;
}


