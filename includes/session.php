<?php

$xmpp_server_sessions = array();

/**
 * Save the new values of a session. This will call hooks to allow
 * contrib modules to add new session storage mechanisms, such
 * as MySQL or Memcached. WARNING: If you implement this hook, make
 * sure that it is fast, or the performance benefit of having faster
 * session storage wrappers will be negated.
 *
 * @param $session
 *   The session that we're storing. Session managers must keep everything
 *   they need to know about sessions within the session itself. All sessions
 *   have a unique integer key in their 'id' field.
 */
function xmpp_server_save_session($session) {
  global $xmpp_server_sessions;

  $ret = module_invoke_all('xmpp_session_store', $session);
  if (!in_array(TRUE, $ret)) {
    $xmpp_server_sessions[$session['id']] = $session;
  }
}

/**
 * Delete a session. Contrib session storage modules should now
 * use the session's 'id' field to remove the session data from
 * it's memory.
 *
 * @param $session
 *   The session array being deleted.
 */
function xmpp_server_delete_session($session) {
  global $xmpp_server_sessions;
  module_invoke_all('xmpp_session_delete', $session);
  unset($xmpp_server_sessions[$session->id]);
}

/**
 * Retrieve a client's session. Modules that provide session storage
 * mechanisms should identify a session by it's unique integer
 * key, stored in it's 'id' field, which will match the 'sessionid'
 * field on the client object.
 *
 * @param $client
 *   The client that owns the session that we're looking for.
 * @return
 *   The client's session. Note that if no session exists, one will
 *   be created.
 */
function xmpp_server_get_session($client) {
  static $id;
  global $xmpp_server_sessions;

  if (!isset($client->sessionid)) {
    $id++;
    $client->sessionid = $id;
    $session['id'] = $id;
    $session['cid'] = $client->id;
    xmpp_server_save_session($session);
  }

  /* Modules should be weighted by order of speed. Fallbacks will be used
   * automatically. */
  foreach (module_implements('xmpp_session_retrieve', true) as $mod) {
    $ret = module_invoke($mod, 'xmpp_session_retrieve', $client);
    //Return our first successful match.
    if (is_array($ret)) {
      return $ret;
    }
  }
  
  return $xmpp_server_sessions[$client->sessionid];
}

/**
 * Get the client object that a session belogs to.
 *
 * @param $session
 *   The session object.
 * @return
 *   The client object that the session object belongs to.
 */
function xmpp_server_session_get_client($session) {
  return xmpp_server_get_client($session['cid']);
}

/**
 * This is the management function for the ID map, which allows contrib
 * XMPPD modules to define custom ways of referring to IDs. For exmaple,
 * The XMPP handler uses this to map JIDs to client IDs.
 * 
 * @param $key
 *   The unique ID specific to the module. This supports * wildcards.
 * @param $type
 *   The 'type' of session that the module uses. This should be the same type
 *   that's stored in the 'type' attribute on the client object.
 * @param $newval
 *   $newval significantly alters the behavior of xmpp_server_id_map()
 *   depending on it's value. If $newval is NULL, the function will attempt to
 *   return the appropriate key. If $newval is 'false', the function will
 *   attempt to delete the appropriate key. Otherwise, the function will
 *   insert/update the specifed key as appropriate.
 * @param $sid
 *   $sid only affects the deletion of a key. If $newval is FALSE and $sid is
 *   true, $key will be assumed to be the system ID to be removed, and will
 *   remove all entries to that key of the specified $type.
 * @return
 *   If $newval is NULL and one or more keys matching the given pattern exist,
 *   they will be returned as an array. Otherwise, this function returns
 *   nothing.
 */
function xmpp_server_id_map($key, $type, $newval = NULL, $sid = false) {
  static $map;
  if (!isset($map)) {
    $map = array();
  }
  
  if ($type == 'uid' && $newval === NULL) {
    $key = "$key/%";
  }
  
  //Allow SQL wildcard matches with *
  $key = str_replace('*', '%', $key);
  
  if ($newval === NULL) { //Retrieve
    $res = db_query("SELECT sid FROM {xmpp_server_id_map} 
      WHERE key_type='%s' 
        AND key_value LIKE '%s'", 
          $type, $key);
    
    $ret = array();
    while ($row = mysql_fetch_object($res)) {
      $ret[] = $row->sid;
    }
    
    return $ret;
    /*
    //Return nothing if there's no such key.
    if (!empty($ret)) {
      if(count($ret) == 1) {
        return $ret[0];
      }
      return $ret;
    }*/
  } 
  elseif ($newval === false) { //Delete
    if ($sid) {
      db_query("DELETE FROM {xmpp_server_id_map} 
        WHERE key_type='%s' 
          AND sid=%d", $type, $key);
    } 
    else {
      db_query("DELETE FROM {xmpp_server_id_map} 
        WHERE key_type='%s' 
          AND key_value LIKE '%s'", $type, $key);
    }
  } 
  else { //Set
    $res = db_query("SELECT COUNT(*) AS count FROM {xmpp_server_id_map} 
      WHERE key_type='%s' 
        AND key_value LIKE '%s'", $type, $key);
    $count = db_fetch_object($res)->count;
    if ($count) {
      db_query("UPDATE {xmpp_server_id_map} SET sid=%d 
        WHERE key_type='%s' 
          AND key_value LIKE '%s'", $newval, $type, $key);
    } 
    else {
      db_query("INSERT INTO {xmpp_server_id_map} (key_type, key_value, sid) 
        VALUES ('%s', '%s', %d)", $type, $key, $newval);
    }
  }
}

