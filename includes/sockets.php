<?php

/**
 * Start our sockets and set up the SSL context from our Drupal config vars.
 */
function xmpp_server_start_sockets() {
  global $xmpp_server_sock;
  
  //Set up our SSL context for encrypted traffic.
  $cx = array(
    'ssl' => array(
      'local_cert' => variable_get('xmpp_server_ssl_local_cert', ''), 
      'cafile' => variable_get('xmpp_server_ssl_cafile', ''),
      'capath' => variable_get('xmpp_server_ssl_capath', ''),
      'passphrase' => variable_get('xmpp_server_ssl_passphrase', ''),
      'verify_peer' => false,
      'allow_self_signed' => true,
    ),
  );

  $cx = stream_context_create($cx);

  $port = variable_get('xmpp_server_port', 5222);
  $addr = variable_get('xmpp_server_address', '0.0.0.0');
  $xmpp_server_sock = stream_socket_server("tcp://$addr:$port", $errno, $errstr, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN, $cx);
  if (!$xmpp_server_sock) {
    $msg = "Could not open socket: @error";
    $vars = array('@error' => $errstr);
    echo t($msg, $vars) . "\n";
    watchdog(XMPP_SERVER_WATCHDOG_TYPE, $msg, $vars, WATCHDOG_ERROR);
    die();
  }

  echo t("XMPP Server sockets online.") . "\n";
}

/**
 * Send data to a client.
 *
 * @param $client
 *   The client we're sending data to.
 * @param $data
 *   The data to send. If this is an object it will be assumed to be an XML 
 *   object. See the documentation on xmpp_server_xml_serialize().
 *   If the data is an array, each value will be sent in turn. This calls
 *   xmpp_server_send() recursively, so DO NOT EVER send recursive arrays.
 *   If the data is a string, it will be sent verbatem.
 */
function xmpp_server_send($client, $data) {
  //Recursively send packets
  if (is_array($data)) {
    foreach ($data as $packet) {
      xmpp_server_send($client, $packet);
    }
    return;
  }

  //Serialize first
  if (is_object($data)) {
    $data = xmpp_server_xml_serialize($data);
  }

  echo "Sending data: $data\n";

  if (is_array($client)) {
    foreach ($client as $c) {
      fwrite($c->sock, $data);
    }
  } 
  else {
    fwrite($client->sock, $data);
  }
}

function xmpp_server_send_to($key, $type, $node, $bare = false) {
  $clients = xmpp_server_id_map($key, $type);
  foreach ($clients as $client) {
    $c = xmpp_server_get_client($client);
    $s = xmpp_server_get_session($c);
    if ($s['gotroster'] || $node->tag != 'presence') {
      if ($node->tag == 'iq') {
        $node->options['id'] = xmpp_server_client_new_id($c);
      }
      $node->options['to'] = xmpp_serialize_jid($s['jid'], $bare);
      xmpp_server_send($c, $node);
    }
  }
}

/**
 * Closes a client's socket.
 *
 * @param $client
 *   The client to close.
 */
function _xmpp_server_close_sock($client) {
  fclose($client->sock);
}

/**
 * Enables or disables encryption on a client state.
 *
 * @param $client
 *   The client with the socket to set.
 * @param $state
 *   Set to TRUE to enable encryption, or FALSE to disable.
 */
function xmpp_server_encrypt($client, $state = true) {
  while (($res = stream_socket_enable_crypto($client->sock, $state, STREAM_CRYPTO_METHOD_SSLv23_SERVER)) === 0);
  if ($res) {
    $client->ssl = $state;
  }
}

/**
 * Process any waiting sockets data, accept new clients, etc.
 */
function _xmpp_server_process_data() {
  global $xmpp_server_sock, $xmpp_server_clients;

  //Temporary array for select() to modify.
  $read = array($xmpp_server_sock);

  foreach ($xmpp_server_clients as $client) {
    $read[] = $client->sock;
  }

  //Select for one second and then give up.
  if (($res = stream_select($read, $w = NULL, $e = NULL, 1)) === 0) {
    return;
  }

  //If there are connections waiting to be accepted...
  if (in_array($xmpp_server_sock, $read)) {
    //Accept it and add it to the global socket list.
    $sock = stream_socket_accept($xmpp_server_sock);

    //Set up a new client.
    $client = xmpp_server_new_client();

    //Set the sock resource to our new socket.
    $client->sock = $sock;

    //Unset the waiting socket so we don't try to read from it later.
    $key = array_search($xmpp_server_sock, $read);
    unset($read[$key]);
  }

  foreach ($read as $v) {
    //Fetch our client object.
    $client = xmpp_server_get_client($v, 'sock');

    //Read in some data.
    $data = @fread($v, 2048);

    if ($data === false || $data === "") {
      xmpp_server_close_client($client);
      continue;
    }

    echo "Got data: $data\n";

    xmpp_server_xml_stream_parse($client, $data);
  }
}

