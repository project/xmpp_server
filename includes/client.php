<?php

$xmpp_server_clients = array();

class xmpp_server_client {
}

/**
 * Create a new client.
 */
function xmpp_server_new_client() {
  global $xmpp_server_clients;

  static $id;
  $id++;

  $p = new xmpp_server_client();
  $p->id = $id;
  $p->_uid = 1;
  $p->xml = xmpp_server_xml_parser();

  $xmpp_server_clients[$p->id] = $p;

  echo "Client created\n";
  module_invoke_all('xmpp_server_new_client', $p);

  return $p;
}

/**
 * Get a client from the global list.
 *
 * @param $val
 *   The value of the key we're looking for.
 * @param $key
 *   The type of ID to match against (default is 'id').
 * @return
 *   The client (or array of clients) that match(es) the key/value
 *   pair on the ID map
 */
function xmpp_server_get_client($val, $key = 'id') {
  global $xmpp_server_clients;
  if ($key == 'id') {
    return $xmpp_server_clients[$val];
  }
  else {
    return get_object_list($xmpp_server_clients, $key, $val);
  }
}

/**
 * Close a client connection.
 *
 * @param $client
 *   The client to close.
 */
function xmpp_server_close_client($client) {
  global $xmpp_server_clients;

  //Give everyone the destruction notification.
  module_invoke_all('xmpp_server_client_close', $client);
  
  //Delete the client's session.
  $s = xmpp_server_get_session($client);
  xmpp_server_delete_session($s);

  //Unset our entries in the ID map.
  xmpp_server_id_map($client->id, 'uid', false, true);
  xmpp_server_id_map($client->id, 'jid', false, true);


  echo "Client destroyed.\n";
  unset($xmpp_server_clients[$client->id]);
}

/**
 * Get a disposable unique ID for this client session.
 * This is useful for XMPP iq stanzas.
 *
 * @param $client
 *   The client we're getting an ID for.
 */
function xmpp_server_client_new_id($client) {
  return "id". $client->id . "-". $client->_uid++;
}

