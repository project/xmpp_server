<?php

/**
 * Loops through an array of objects or arrays looking for a match 
 * where $object->$key == $val.
 *
 * @param $list
 *   The array through which to loop.
 * @param $key
 *   The name of the key to look in.
 * @param $val
 *   The value that this key must be in order to match.
 */
function get_object_list($list, $key, $val) {
  foreach ($list as $p) {
    if (is_object($p)) {
      if ($p->$key == $val) {
        return $p;
      }
    } 
    elseif (is_array($p)) {
      if ($p[$key] == $val) {
        return $p;
      }
    }
  }
  return false;
}

/**
 * Get a user id by it's jid.
 *
 * @param $jid
 *   The JID to search for.
 * @return
 *   The UID of the user if one exist, or FALSE otherwise.
 */
function xmpp_get_uid_by_jid($jid) {
  $uname = '';
  if (is_array($jid)) {
    $uname = $jid['name'];
  } 
  else {
    $jid = xmpp_parse_jid($jid);
    $uname = $jid['name'];
  }
  $res = db_query("SELECT uid FROM {users} WHERE name='%s'", $uname);
  while ($row = db_fetch_object($res)) {
    return $row->uid;
  }
  return false;
}

/**
 * Serialize a jid into it's URI form.
 *
 * @param $jid
 *   The JID object, consisting of 'name', 'domain' and, optionally, 'resource'
 *   fields.
 * @param $bare
 *   If false, render the full JID. If true, do not append the resource to the 
 *   JID. Otherwise, append a wildcard resource identifier.
 * @return
 *   The JID, serialized into a string.
 */
function xmpp_serialize_jid($jid, $bare = false) {
  if ($bare === NULL) {
    return "{$jid['name']}@{$jid['domain']}/*";
  }
  elseif ($jid['resource'] && $bare === false) {
    return "{$jid['name']}@{$jid['domain']}/{$jid['resource']}";
  } 
  else {
    return "{$jid['name']}@{$jid['domain']}";
  }
}

/**
 * Parse a string into a JID object.
 *
 * @param $jid
 *   The JID to parse.
 * @return
 *   The JID object.
 */
function xmpp_parse_jid($jid) {
  $ret = array();
  list($ret['name'], $ret['domain'], $ret['resource']) = sscanf($jid, '%[^@]@%[^/]/%s');
  return $ret;
}

