<?php

define('XMPP_SERVER_WATCHDOG_TYPE', 'xmppd');

//Begin XMPPD bootstrapping
if (!file_exists('xmppd.ini')) {
  echo "Please make sure your xmppd.ini file exists and is readable by the current user.\n";
  if (file_exists('xmppd.ini.example')) {
    echo "It looks like you haven't configured the server yet. Please look at xmppd.ini.example and README.txt for more information.\n";
  }
  die();
}

/* Parse our very simple config file.
 * Where previously almost all of our settings were
 * in here, now they are in Drupal's admin pages. */
$ini = parse_ini_file('xmppd.ini', false);

//Fool Drupal into thinking it's being run through the web.
$_SERVER['HTTP_HOST'] = $ini['http_host'];
$_SERVER['SCRIPT_NAME'] = $ini['uriroot'] . '/index.php';
$_SERVER['REMOTE_ADDR'] = NULL; // any values here do rather...
$_SERVER['REQUEST_METHOD'] = NULL; // ...odd things. uh huh.

if (!file_exists($ini['path']) || !is_dir($ini['path'])) {
  die("ERROR: Please set the 'path' variable in the 'Drupal' section of your xmppd.ini to the root of your Drupal installation.\n");
}

//Change to our Drupal directory.
chdir($ini['path']);

//Unset the base url just in case.
unset($base_url);

//Include and bootstrap the Drupal system
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//Assert the presence of the XMPP Server module
if (!module_exists('xmpp_server')) {
  $msg = "Please make sure that the XMPP Server module is installed in the site @host.";
  $vars = array('@host' => $ini['http_host']);
  watchdog(XMPP_SERVER_WATCHDOG_TYPE, $msg, $vars, WATCHDOG_ERROR);
  echo t($msg, $vars) . "\n";
  die();
}

//Set our memory limit.
ini_set('memory_limit', variable_get('xmpp_server_memory_limit', '128M'));

module_invoke_all('xmpp_server_start');

//We use this to refresh our Drupal variables.
$time = 0;

//set_error_handler('xmppd_error_handler');

//Our server operates off the xmpp_server_running variable.
variable_set('xmpp_server_running', true);
while (variable_get('xmpp_server_running', true)) { //Main loop

  //Every 30 seconds...
  if (time() > $time + variable_get('xmpp_server_refresh_time', 30)) {
    //Refresh all our variables.
    $conf = variable_init();
  }

  //Check it out. That's it. Our whole main loop.
  xmpp_server_run_loop();

  //And... breathe.
  usleep(100);
}

module_invoke_all('xmpp_server_halt');

function xmppd_error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
  $ignore = array(2048, 2, 8);
  if (in_array($errno, $ignore)) {
    return;
  }

  $bt = debug_backtrace();
  echo "Error $errno: $errstr\n";
  foreach($bt as $fcall) {
    echo " * {$fcall['file']}:{$fcall['line']} => {$fcall['function']}()\n";
  }
}
